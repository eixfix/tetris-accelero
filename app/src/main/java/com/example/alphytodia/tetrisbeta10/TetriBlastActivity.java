package com.example.alphytodia.tetrisbeta10;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.app.Dialog;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class TetriBlastActivity extends Activity implements SensorEventListener{
    
	private MainMap mMainMapView;

	public static final String TAG = "TetrisBlast";
    
	private static String ICICLE_KEY = "tetris-blast-view";
    private SensorManager sensorManager;

	/** Called when the activity is first created. */
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);
        Log.d(TAG, "Create main layout");
        getWindow().setBackgroundDrawableResource(R.drawable.tetris_bg);//Draw background
        sensorManager=(SensorManager)getSystemService(SENSOR_SERVICE);
// menambahkan listener. Listener untuk class ini adalah accelerometer_3axis
        sensorManager.registerListener(this,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL); // fungsi api yang dipakai untuk perubahan screen orientation
        mMainMapView = (MainMap) findViewById(R.id.tetris);
        mMainMapView.initNewGame();
        //TextView myText = (TextView) findViewById(R.id.txt);
        
        if (savedInstanceState == null) {
            // We were just launched -- set up a new game
        	mMainMapView.setMode(MainMap.READY);
        } else {
            // We are being restored
            Bundle map = savedInstanceState.getBundle(ICICLE_KEY);
            if (map != null) {
            	mMainMapView.restoreState(map);
            } else {
            	mMainMapView.setMode(MainMap.PAUSE);
            }
        }
    }
    public void onAccuracyChanged(Sensor sensor,int accuracy){

    }
    public void onSensorChanged(SensorEvent event){

// cek jenis sensor
        if(event.sensor.getType()==Sensor.TYPE_ACCELEROMETER){
            Log.d(TAG, "move left x = " +event.values[0]);
            Log.d(TAG,"move left y = " + event.values[1]);
            mMainMapView.setMode(MainMap.READY  );
        }
    }
        @Override
    protected void onPause() {
        super.onPause();
        // Pause the game along with the activity
        mMainMapView.setMode(MainMap.PAUSE);
    }
    
    @Override
    protected void onStop() {
        super.onPause();
        // Pause the game along with the activity
        mMainMapView.setMode(MainMap.PAUSE);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        //Store the game state
        outState.putBundle(ICICLE_KEY, mMainMapView.saveState());
    }
}